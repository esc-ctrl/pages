# Workshop
ESC-CTRL <esc-ctrl@systemli.org>
v2.1, 2020-02-25
:revealjsdir: reveal.js
:imagesdir: img
:revealjs_totalTime: 2700

---
Bedrohungsmodellierung für Aktivist:innen

---

++++
<style>
.reveal section img { background:none; border:none; box-shadow:none; }
.schema p { margin: 0; }
</style>
++++

## Vorab-Infos

[.step]
* Geht die Technik bei allen?
* Vorsicht Aufnahmen möglich
* Kein sicherer Raum

[.notes]
--
* Alle da?
* Selbst entscheiden was zeigen

--

## Chat Zeichen

[cols="1,5"]
|===
| *
| Meldung

| ?
| Frage

| +
| Zustimmung

| -
| Ablehnung

| 8
| Wiederholung
|===

## Vorstellungsrunde

* Personen
* Erfahrungen zum Thema
* Erwartungen vom Workshop

[.notes]
--
* Name/Pronomen
* CryptoPartys
* IT Hintergrund / Fachwissen
* Aktivistischen Perspektive
* Webinar ist Neuland
--

## Fahrplan

[.schema, cols="6,2"]
|===
| Input
| 45 min.

| Pause / Gruppenfindung
| 10 min.

| Praxis
| 60 min.

| Pause
| 10 min.

| Reflektion
| 15 min.

| Tipps
| 5 min.
|===

[.notes]
--
* Erfahrungen austauschen
* Verständnisfragen sofort
* Dikussion in Kleingruppen
* Beiträge: Gesprächsverhalten reflektieren
* Beiträge: Fachbegriffe erklären
* Noch Wünsche zum Umgang?
* Inhalt orientiert an Bedrohungsmodelierung
--

## Was ist eine Bedrohungsmodellierung?

[.notes]
--
* Schema zum Anwenden
* Hilft Bedrohungen zu Identifizieren und Gegenmaßnahmen zu priorisieren
* Entwickelt im US-Militär Kontext, "Operational Security"
* Verwendet im IT Bereich
--

### Konzept

[.step]
* Schrittweise wiederholbar
* Übertragbar auf Aktivismus
* Nur ein Teil von Sicherheit

[.notes]
--
* Nur technische Sicherheit
* Mehr Gründe warum etwas schief geht (Wohlbefinden etc.)
* „Holistic Security“ (ganzheitlichen Sicherheite) versucht menschliche Komponenten zu berücksichtigen
--
### Notwendigkeit

[.step]
* Daten sind Macht
* Macht wird missbraucht
* Verhältnisse ändern sich
* Solidarität schützt

[.notes]
--
**Daten sind Macht**

* Massenhaft produziert und erhoben
* Zwangsweise mit Menschen/Institutionen geteilt
* In der Breite durch Unternehmen oder gezielt durch Ermittlungsbehörden
* Wer weiß was passiert kann das einfacher verhindern
* Wer Schwächen kennt kann diese ausnutzen
* Wer Geheimnisse kennt kann erpressen
* Wenn wir kein Vertrauen in die Personen/Institutionen haben, dann sollten wir keine Daten geben
* Gründe warum wir selbst wenn wir Vertrauen haben, vorsichtig sein sollten:

**Macht wird Missbraucht:**

* Daten werden entwendet/geleakt (Bsp. sind Drohbriefe dei November 2017 an Linke Szene Lokale verschickt wurden und LKA Daten beeinhalteten, wie ED-Bilder)
* Beispiel: Polizei Abfragen 2019 zu Helene Fischer

**Verhältnisse ändern sich:**

* Gesellschaft ändert sich
* Charakter der Institution verändert
* Vertrauensverhältnis ändert sich
* Historisches beispiel: Rosa Liste, welche zur Keiserzeit Homosexuelle Dokumentiert hat, danach Nazis	* Nazis bekommen Register der Religionszugehörigkeit Niederlande 1936
* Fazit: Was ich heute für unbedenklich halte, sieht morgen anders aus 


**Solidarität schützt:**

  * Assoziiert mit Menschen
  * Eine Schwachstelle im Netzwerk ist Gefahr
  * Großes Rauschen
  * Privilegien: Kann es mir leisten mich immer auszuweisen, weil ich Deustchen Pass habe
  * Wichtig: Aktiv gegen Repression, Verantwortung übernehmen für die Sicherheit der Gruppe
--

### Schema

[.schema, grid="none", frame="none", cols="1,5", header="none"]
|===

| image:cake.svg[static, 30]      | 1. Assets
| image:cloud.svg[static, 30]     | 2. Gegner:innen
| image:rain.svg[static, 30]      | 3. Bedrohungen
| image:risk.svg[static, 30]      | 4. Risiken
| image:umbrella.svg[static, 30]  | 5. Gegenmaßnahmen
|===


[.notes]
--

Asset = zu schützendes gut (Daten oder Dinge)

1. Was wollen wir schützen?
2. Vor wem wollen wir es schützen?
3. Welche Bedrohungen sind zu erwarten?
4. Wie wahrscheinlich bzw. schwerwiegend sind die Bedrohungen?
5. Wie können die größten Risiken abgewendet werden?

Sehr vereinfacht:

* Torte mit Kerze
* Wetter
* Regen (zu heiß wäre auch nicht gut)
* Abschätzen (Wetterbericht)
* Regenschirm
--


## Assets

Was wollen wir schützen?

image::cake.svg[static, 200]

[.notes]
--
* Informationen oder (Wert-)Gegenstände
* Subjektiv, eure Entscheidung
* Je mehr desto besser
* Aber, je mehr desto komplexer das Modell
* Nach Wichtigkeit sortieren
--

### Identitäten

* Bürgerliche Identität (Name etc.)
* Organisationsstrukturen
* Verhaltensmuster
* Gerätekennungen
* ...

[.notes]
--
* ID wird von Gegner:innen gerne ausgespät um Druck aufzubauen, Aktionen vorzubereiten
* Bsp.: Nazi-Prepper bereiten "Tag-X" vor
--

### Gegenstände

* Dokumente
* Datenträger
* Werkzeuge
* ...

### Weitere Informationen

* Absichten / Ziele
* Wissensstand
* Skillsets
* Soziale Netzwerke
* ...

## Gegner:innen

Vor wem wollen wir es schützen?

image::cloud.svg[static, 200]

[.notes]
--
* Unsere Gegenspieler:innen
* Personen / Institutionen
--

### Akteur:innen

#### Privatpersonen & Gruppen

* Umweltzerstörer:innen
* Rassist:innen
* Nationalist:innen
* Klimawandelleugner:innen

[.notes]
--
* Zufällig involviert
* Politische Motivation
* Wirtschaftliche Motivation
* Bsp.: Naziangriffe in Neukölln auf politisch Engagierte
--

### Akteur:innen

#### Firmen (Aktiv)

* PR-Abteilung
* Sicherheitsabteilungen
* Anwält:innen
* Securities

[.notes]
--
* Wirtschaftliche Interessen
* Großes Feld, unterschiedliche Kernkompetenzen
* Verhindern, Aufklären, Verfolgen
* Fall 5: EDF Greenpeace, Atom Unternehmen beauftragt Sicherheitsfirma Informationen zu Greenpeace Anti-Atom Kampagne zu  bekommen, diese wendet auch illegale Methoden (Hacking) an
--

### Akteur:innen

#### Firmen (Passiv)

* Suchmaschinen
* Social Networks
* Telefonanbieter:innen
* ...

[.notes]
--
* Infrastruktur
* Sammeln & speichern Daten
--


### Akteur:innen

#### Staat

* Schutz-/ Bereitschaftspolizei
* Kriminalamt (LKA, BKA)
* Verfassungsschutz (Bund und Länder)
* MAD, BND

[.notes]
--
* Durch Monopolisierung von Gewalt/Macht immer dabei
* SchuPo / BePo meist der Erstkontakt
* Kriminalämter: Landes/Bundesoberbehörden
* LKA und BKA haben Staatsschutzabteilungen in Berlin LKA 5 (Dezernat 52) für links
* Verfassungsschutz: Inlandsgeheimdienst
* Unterschiede in Absicht/Fähigkeiten
* MilitärischerAbschirmDienst: Bundeswehr
* Bsp.: Fall 4 - Franzözische Agent:innen versenken Greenpeace Schiff Rainbow Warrior vor Neuseeland
--

### Fähigkeiten

#### Durchsuchungen

* Personenkontrollen
* Fahrzeugkontrollen
* Hausdurchsuchungen
* Hausmülldurchsuchungen

[.notes]
--

**Personenkontrollen**
* Identifikation (keine Anonyme Aktion/Camp mehr)
* Transpi etc. in der Tasche = Struktur

**Fahrzeugkontrollen**
* Identifikation
* Einordnung "politisch Links" bei Demos

**Observationen**
* Kontaktpersonen
* Bewegungsprofil

**Hausdurchsuchungen**
* Nachweisen von Straftaten
* Fall 16: Adbusting in Berlin, Durchsuchung 2019 weil Plakate mit antimilitaristischen Inhalten aufgehängt wurden
* Fall 18: 3 Durchsuchungen Berlin 2020 wegen Störung für Aufmerksamkeit für Rojava, auch schon 2019

**Hausmülldurchsuchungen**
* Verrät sehr viel
* Fall 3: Aus "Schöner leben ohne Spitzel" jemand bot in England mehreren Gruppen an Papiermüll zu recyclen und gab infos weiter

--

### Fähigkeiten

#### Technologische Überwachung

* Fotos/Videos
* Tonaufnahmen
* Datenauswertungen

[.notes]
--

**Videos (versteckt)**
* Oder Treffen von Linksunten Indeymedia auch der Eingang abgefilmt

**Videos (offen)**
* Demos
* Überwachungskameras
* Kamerawagen

**Tonaufnahmen**
* Wanze (Hausdurchsuchung) Vermutet wird das z.B. bei Linksunten im Einsatz
* Richtmikrofon (In den Technikwagen der Polizei)

**Datenauswertung**
* Verschiedene Quellen
* Bsp: Gesichtserkennung G20 (Verfolgung, nicht Identifikation)
* INPOL Datenbank (ED* Behandlungen) hat fünf Mio. Lichtbilder
* 40.000 Recherchen in DB 2018, ca. 1000 identifizierte 2018

--

### Fähigkeiten

#### Forensik

* Faserspuren
* Schuhabdrücke
* Fingerabdrücke
* DNA
* ...

[.notes]
--

**Fingerabdrücke:**
* Fall 17: Adbusting gegen AfD (Höcke) 2016, wurden Fingerabdrücke genommen und auch DNA Analysen

**DNA**:
* Immer öfter weil billiger
* Kann Angeordnet werden
--

### Fähigkeiten

#### Persönliche Überwachung

* Zielfahnder:innen
* Tatbeobachter:innen
* Szenekundige Beamt:innen
* Verdeckte Ermittler:innen
* Vertrauenspersonen

[.notes]
--
* Zielfahnder:innen suchen & observieren Personen

**VEs, Verdeckte Ermittler:innen**
* Wenig inhaltliche Positionierung, vorallem übernehmen von Infrastruktur Aufgaben
* Nur Zeit bei „spannenden Sachen“
* Fall 1: Simon Brenner, fast 1 Jahr in Heidelberg aktiv um Umfasendes Szeneprofil zu erstellen. Vorallem im BEreich ANtifa und Umwelt aktiv. Aufgeflogen wegen einer Urlaubsbekanntschaft, die seine echte Identität kannte.
* Einige im Umfeld der Roten Flora Anfang 2000, teilweise Jahrelang aktiv

**Zivilpolizei**
*  Westen und Knopf im Ohr, Rand der Demo

**V-Personen**
* werden vom Staat bezahlt
* Sitzen im Plenum und wissen über Aktion/Leute
* Fall 2 Gerrit Greiman: 2016-18 Göttingen. An Uni aktiv und in der IL (Basisdemokratische Linke), Ziel waren Göttinger Strukturen
* schwer zu erkennen, weil keine Lücke im Lebenslauf

**Zahlen:**

Nach Hochrechnung von 2011:
* 50 Berliner VS-Beschäftigten, die sich mit der Linken von Linkspartei über ASten bis zur Interim befassen
* 14-28 V-Personen
* Plus: Spitzel des BfV + VE vom LKA und BKA + ausländische VEs
* ca. 50 V-Personen und VEs

--

### Fähigkeiten

#### Überwachung von Computern

* (Staats)Trojaner
* Verbindungsdatenabfrage
* Anfragen bei Anbieter:innen

[.notes]
--

**(Staats)Trojaner** = Online Durchsuchung

* Nicht Quellen TKÜ, die nur Kommunikation
* Jahre lang Diskutiert
* Rechtliche Grundlage besteht und angeblich fertig entwicklet
* Kein dokumentierter Einsatz

**Verbindungsdaten** = Vorratsdatenspeicherung

* IP, Standort, Verbindungsdaten für min. 6 Monate
* Rechtlich umstritten (2017 abgewehrt)
* Beschlossen, aber wird nicht durchgesetzt, bis letztlich geklärt
* Der Arbeitskreis Vorratsdatenspeicherung hat herausgefunden, dass viele Provider folgende Daten speichern: Standort – 1 Woche lang, IMEI bis zu vier Monate lang und die IP-Adresse bis zu drei Monate lang (liste digitalcourage)

* Anfragen bei Mailprovider etc: Fall 12 RAZ wurden Mailprovider angefragt und haben Mailinhalte zur Verfügung gestellt. Hat bei PGP verschlüsselten Mails nichts gebracht.

--

### Fähigkeiten

#### Überwachung von Handys

* Funkzellenabfrage
* Stille SMS
* Verbindungsdatenabfrage
* Telekommunikationsüberwachung
* (Staats)Trojaner

[.notes]
--
**Funkzellenabfrage**
* Standart: Berlin 2018 -> 572. Berlin 2017 -> 474 = 59 Millionen Verkehrsdatensätze. 2.222 mal Inhaber der Handynummer ermittelt

**Stille SMS**
* Bei Polizei vorallem die Länder, bei Verfassungsschutz der Bund
* Bei Polizei zur Festnahme
* Berlin Polizei 2019 ca. 350.000
* ca. 400 SMS pro Maßnahme bei Polizei
* grob 875 Fälle
* Bei Verfassungsschutz für Bewegungsprofile
* 2017 BfV über 400.000

* Verbindungsdaten = Vorratsdatenspeicherung
* TKÜ: Mithören und Mitlesen über Schnittstelle von Hersteller/Anbieter
* Trojaner
* IMSI Catcher, Digitale Forensik, direktes mithören ... später genauer
--

## Bedrohungen

Welche Bedrohungen sind zu erwarten?

image::rain.svg[static, 200]

[.notes]
--
Ergibt sich aus den Fähigkeiten der Gegner:innen

--
## Risiken

Wie wahrscheinlich bzw. schwerwiegend sind die Bedrohungen?

image::risk.svg[static, 200]

[.notes]
--
* Zweidimensional: Wahrscheinlichkeit und Bedrohlichkeit
--

## Gegenmaßnahmen

Wie können die größten Risiken abgewendet werden?

image::umbrella.svg[static, 200]

### Inhalte verschlüsseln

* Datenträger
* Kommunikation
* ...

### Metadaten entfernen

* Internet
* Dateien
* Drucker
* Handys
* WLAN, BT und GPS
* ...

### Sicherere Software

* Open Source
* Updates
* Lokal
* Starke Passwörter
* ...

### Praktisches

* Sauber arbeiten
* Aufräumen
* ...

[.notes]
--
* Nichts zuhause liegen haben (Zettel, Kleidung ...)
* Spuren vermeiden
--

### Organisatorisches

* Compartmentalization
* Bewusstsein
* Sicherheitsstandard
* ...

[.notes]
--
* Trennen der Bereiche (Name, Kleidung, User)
* Sicherheitsstandart kann schwer im Nachhinein erhöht werden
* Sollte bei allen im Zusammenhang gleich sein

--

### Vorbereitungen

* Canarys
* Notfallplan
* ...

### Informationssicherheit

* Datensparsamkeit
* Need to know
* Bescheidenheit
* ...

### Nachbereitung

* Juristisches
* Öffentlichkeit schaffen
* Reflektion
* ...

## Typische Fails

[.step]
* Selbstüberschätzung
* Vertrauen
* Unterschätzte Risiken
* Guilt by association
* Klartext
* Dokumentation

[.notes]
--
**Selbstüberschätzung:**

*  „Erwischen mich eh nicht“

**Vertrauen:**

* Tendenziell Gefährlich, aber ohne gehts nicht
* Notwendig für Handlungsfähigkeit
* Abwägung

**Risiko Unterschätzung:**

* Unterschätzung der Risiken von bereits identifizierten Gefahren
* Totalüberwachung im Internet von Community theoretisch bewusst, aber bis Snowden nicht ernst genommen
* Tor Exit Nodes Überwachung
* Beispiel: Handys von Sprayern, zwar Aktionshandy, aber nichts gewechselt, irgendwann auf dem Schirm

**Guilt by association:**

* Beispiel: Andrej Holm, Gentrifizierung -> Militante Gruppe

**Klartext:**

* SMS und Email wie Postkarte

**Dokumentation:**

* Keine Pics auf dem Privathandy aus der Blockade

--
## Praxis

## Reflektion

* Today I learned
* Offene Fragen
* Kritik

## Links

### Anleitungen

* https://capulcu.blackblogs.org/neue-texte/bandi/[Tails Broschüre]
* https://ssd.eff.org/[Surveillance Self-Defense]
* https://tacticaltech.org/#/projects/holistic-security[Holistic Security ]
* https://tacticaltech.org/#/projects/security-in-a-box[Security In-a-box]
* https://www.privacy-handbuch.de/index.htm[Privacy Handbuch]
* https://www.ccc.de/en/campaigns/aktivitaeten_biometrie/fingerabdruck_kopieren[Fingerabdrücke Faken CCC]
* https://militanz.blackblogs.org/wp-content/uploads/sites/481/2017/11/Prisma.pdf[Prisma Kapitel: "Unsere eigene Sicherheit"]

### Initiativen

* https://tacticaltech.org/#/[Tacicaltech]
* https://www.eff.org/[Electronic Frontier Foundation]
* https://digitalcourage.de/[Digital Courage]

### Inhalte/Analysen

* https://capulcu.blackblogs.org/[Capulcu Kollektiv zum technologischen Angriff]
* https://netzpolitik.org/2017/chronik-des-ueberwachungsstaates/[Chronik des Überwachungsstaates]
* https://www.datenschmutz.de/moin/[Datenschmutz Wiki]

### Tipps

* https://riseup.net/en/security/resources/radical-servers[Radical Servers]
* https://www.cryptoparty.in/berlin[Crypto Party]
* https://prism-break.org/en/[Prism Break]

### Bücher

* https://www.assoziation-a.de/buch/Spitzel[Spitzel * Eine kleine Sozialgeschichte]
* https://www.edition-assemblage.de/buecher/wege-durch-die-wueste/[Wege durch die Wüste]
* https://www.unrast-verlag.de/gesamtprogramm/allgemeines-programm/anarchie-autonomie/kontrollverluste-298-detail[Kontrollverluste * Interventionen gegen Überwachung]
* http://schwarzerisse.de/[Handbuch Biometrie Hacken]

### Filme zum Thema

* http://www.iminnerenkreis-doku.de[Im inneren Kreis]
* https://citizenfourfilm.com/[Citizen four]

## Kontakt

esc-ctrl@systemli.org

7ED1 AF68 C744 A159 B07E

3DB5 EA15 D608 2A8D CDAA
